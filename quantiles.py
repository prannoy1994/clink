import numpy as np
import csv

'''  
Functions used to calculate quantiles and locate where a given number is located
         
def quantiles(a,n):    #a is the list for which quantiles need to be calculated and n is the number of quantiles
    l=len(a)
    b=(l*(np.arange(1,n)/float(n))).astype(int)
    y=sorted(a)
    q=[]
    for j in range(0,n-1):
        q.append(y[b[j]])       
    return q  

def quantilenum(a,l):
    n=len(l)
    for i in range(0,n):
       if(a<l[i]):
          return i+1
    return n+1

x=[5, 8, 4, 4, 6, 3, 8]
a=quantiles(x,4)
print quantilenum(8,a)
'''

cr = csv.reader(open("/media/prannoy/OS/basics/Transaction Data Dump1.csv","rb"))

row_y=04
row_x=16
nq=10       #Set the number of quartiles you want to calculate

a=[]
b=[]
i=0
c=0
for row1 in cr:
    if i==0:
        i=1
        header1=row1[row_y]
        header2=row1[row_x]
    elif (row1[14]=="IN" and row1[8]=="00"):
        if (row1[row_y]!="" and row1[row_x]!=""):
            a.append(row1[row_y])
            b.append(row1[row_x])

yaxis=set(a)
xaxis=set(b)
y_axis={'Dummy':0.0}
x_axis={'Dummy':0}

for j in yaxis:
    y_axis[j]=0.0
for k in xaxis:
    x_axis[k]=0
del y_axis['Dummy']
del x_axis['Dummy']
#print x_axis
#print y_axis

a=0.0
table={'Dummy':0}
for a in y_axis:
    for b in x_axis:
        table[(a,b)]=0
del table['Dummy']     
#print table
#for calculating amount spent in various super categories and hour
cr = csv.reader(open("/media/prannoy/OS/basics/Transaction Data Dump1.csv","rb"))
i=0
for row in cr:
    if i==0:
        i += 1
    elif (row[14]=="IN" and row[8]=="00"):
        a=float(row[7].replace(",",""))
        if (row[row_y]!="" and row[row_x]!=""):                        
            y_axis[row[row_y]]+= 1
            table[(row[row_y],row[row_x])]+=1
            x_axis[row[row_x]] += 1

#Writing into csv   
c = csv.writer(open("/media/prannoy/OS/basics/{}_{}.csv".format(header1,header2), "wb"))    
x=[]   
y=[]
y.append("{}/{}".format(header1,header2))
x_axis=sorted(x_axis.keys())
y_axis=sorted(y_axis.items(),key=lambda x: x[1],reverse=True)

for i in x_axis:
    y.append(i)
    
c.writerow(y)
for i,k in y_axis:
    x.append(i)
    for j in x_axis:
        x.append((table[(i,j)]))
    c.writerow(x)
    x=[]


def quantiles(a,n):    #a is the list for which quantiles need to be calculated and n is the number of quantiles
    l=len(a)
    b=(l*(np.arange(1,n)/float(n))).astype(int)
    y=sorted(a)
    q=[]
    for j in range(0,n-1):
        q.append(y[b[j]])       
    return q  
l=len(xaxis)+1
limits={}
for j in range(1,l):
   q=[]
   #count=0
   i=0
   c = csv.reader(open("/media/prannoy/OS/basics/{}_{}.csv".format(header1,header2), "rb"))
   for row in c:
        if i==0:
           i = 1
        else:
           if int(row[j])!=0:
              
              q.append(int(row[j]))
              #count+=1
        #if count>7000:             #Flexibilty to remove the set of customers with very low transactions
          #break
   #print q
   limits[j]=quantiles(q,nq)
#print limits
  
def quantilenum(j,a):
    l=limits[j]
    n=len(l)
    for i in range(0,n):
       if(a<l[i]):
          return i+1
    return n+1

     
      
cr = csv.writer(open("/media/prannoy/OS/basics/quartiles_categories.csv", "wb"))    


    
cr.writerow(y)
t=0
f=[]
c = csv.reader(open("/media/prannoy/OS/basics/{}_{}.csv".format(header1,header2), "rb"))
for row in c:
	if t==0:
	   t = 1
	else:
            x=[]
            x.append(row[0])
            for r in range(1,l):
                 qn=quantilenum(r,int(row[r]))
                 x.append(qn)         
            cr.writerow(x)

