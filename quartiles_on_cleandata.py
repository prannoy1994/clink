import csv
def quartiles(x):
	y=sorted(x)
	r=y[::-1]
	z=(len(x)/2)
	if len(x)%2==0:
	   median =(y[z-1]+y[z])/2.0
	else:
	   median=y[z]
	if z%2==0:
	    lq=(y[z/2-1]+y[z/2])/2.0
	    hq=(r[z/2-1]+r[z/2])/2.0
	else:
	    lq=y[z/2]
	    hq=r[z/2]
	qlimits=[lq,median,hq]
        return qlimits
l=13        #Number of columns in the table
limits={}
filepath="/media/prannoy/OS/basics/rc1_90d_trn_dtl.csv"
outfilepath="/media/prannoy/OS/basics/quartiles1_90_categories.csv"
h=["Index","Customer_id"] #stores all category names


for j in range(2,l):              #starting from 2 here because first two rows are index and customer id   
   q=[]
   i=0
   c = csv.reader(open(filepath, "rb"))
   for row in c:
        if i==0:
           h.append(row[j])
           i = 1
        else:
           #if int(row[j])!=0:

                q.append(int(row[j]))
 
   limits[j]=quartiles(q)

  
print "Done here"
def quartilenum(j,a):
     k=limits[j]
     if a<k[0]:
        return 1
     elif a<k[1]:
        return 2
     elif a<k[2]:
        return 3
     else:
        return 4
      
cr = csv.writer(open(outfilepath, "wb"))    
cr.writerow(h)
t=0
f=[]
c = csv.reader(open(filepath, "rb"))
for row in c:
	if t==0:
	   t = 1
	else:
            x=[]
            x.append(row[0])
            x.append(row[1])
            for r in range(2,l):
                 qn=quartilenum(r,int(row[r]))
                 x.append(qn)         
            cr.writerow(x)

