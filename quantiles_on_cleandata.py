import csv
import numpy as np
def quantiles(a,n):    #a is the list for which quantiles need to be calculated and n is the number of quantiles
    l=len(a)
    b=(l*(np.arange(1,n)/float(n))).astype(int)
    y=sorted(a)
    q=[]
    for j in range(0,n-1):
        q.append(y[b[j]])       
    return q
l=13        #Number of columns in the table
qn=04        #Quantile Number
limits={}
filepath="/media/prannoy/OS/basics/rc1_90d_trn_dtl.csv"
outfilepath="/media/prannoy/OS/basics/quartiles1_90_categories.csv"
h=["Index","Customer_id"] #stores all category names


for j in range(2,l):              #starting from 2 here because first two rows are index and customer id   
   q=[]
   i=0
   c = csv.reader(open(filepath, "rb"))
   for row in c:
        if i==0:
           h.append(row[j])
           i = 1
        else:
           #if int(row[j])!=0:

                q.append(int(row[j]))
 
   limits[j]=quantiles(q,qn)

def quantilenum(j,a):
    l=limits[j]
    n=len(l)
    for i in range(0,n):
       if(a<l[i]):
          return i+1
    return n+1
      
cr = csv.writer(open(outfilepath, "wb"))    
cr.writerow(h)
t=0
f=[]
c = csv.reader(open(filepath, "rb"))
for row in c:
	if t==0:
	   t = 1
	else:
            x=[]
            x.append(row[0])
            x.append(row[1])
            for r in range(2,l):
                 qn=quantilenum(r,int(row[r]))
                 x.append(qn)         
            cr.writerow(x)
