'''
Created on 25-Dec-2013

@author: PRANNOY.P
'''
import csv
cr = csv.reader(open("/media/prannoy/OS/basics/Transaction Data Dump1.csv","rb"))

row_y=04
row_x=16

a=[]
b=[]
i=0
c=0
for row1 in cr:
    if i==0:
        i=1
        header1=row1[row_y]
        header2=row1[row_x]
    elif (row1[14]=="IN" and row1[8]=="00"):
        if (row1[row_y]!="" and row1[row_x]!=""):
            a.append(row1[row_y])
            b.append(row1[row_x])

yaxis=set(a)
xaxis=set(b)
y_axis={}
x_axis={}

for j in yaxis:
    y_axis[j]=0.0
for k in xaxis:
    x_axis[k]=0

#print x_axis
#print y_axis

a=0.0
table={}
for a in y_axis:
    for b in x_axis:
        table[(a,b)]=0     
#print table
#for calculating amount spent in various super categories and hour
cr = csv.reader(open("/media/prannoy/OS/basics/Transaction Data Dump1.csv","rb"))
i=0
for row in cr:
    if i==0:
        i += 1
    elif (row[14]=="IN" and row[8]=="00"):
        a=float(row[7].replace(",",""))
        if (row[row_y]!="" and row[row_x]!=""):                        
            y_axis[row[row_y]]+= 1
            table[(row[row_y],row[row_x])]+=1
            x_axis[row[row_x]] += 1

#Writing into csv   
c = csv.writer(open("/media/prannoy/OS/basics/{}_{}.csv".format(header1,header2), "wb"))    
x=[]   
y=[]
y.append("{}/{}".format(header1,header2))
x_axis=sorted(x_axis.keys())
y_axis=sorted(y_axis.items(),key=lambda x: x[1],reverse=True)

for i in x_axis:
    y.append(i)
    
c.writerow(y)
for i,k in y_axis:
    x.append(i)
    for j in x_axis:
        x.append((table[(i,j)]))
    c.writerow(x)
    x=[]


def quartiles(x):
	y=sorted(x)
	r=y[::-1]
	z=(len(x)/2)
	if len(x)%2==0:
	   median =(y[z-1]+y[z])/2.0
	else:
	   median=y[z]
	if z%2==0:
	    lq=(y[z/2-1]+y[z/2])/2.0
	    hq=(r[z/2-1]+r[z/2])/2.0
	else:
	    lq=y[z/2]
	    hq=r[z/2]
	limits=[lq,median,hq]
        return limits

def quartilenum(j,a):
     k=j
     if a<k[0]:
        return 1
     elif a<k[1]:
        return 2
     elif a<k[2]:
        return 3
     else:
        return 4

l=len(xaxis)+1
i=0
count=0
cr = csv.writer(open("/media/prannoy/OS/basics/quartiles_customers.csv", "wb"))
cr.writerow(y)
c = csv.reader(open("/media/prannoy/OS/basics/{}_{}.csv".format(header1,header2), "rb"))
for row in c:
     limits=[]
     if i==0:
         i = 1
     else:
         q=[]
         x=[]
         x.append(row[0])
         for j in range(1,l):
            if int(row[j])!=0:
              q.append(int(row[j]))
         limits=quartiles(q)
         for j in range(1,l):
               qn=quartilenum(limits,int(row[j]))
               x.append(qn) 
         cr.writerow(x)
         count+=1
         #print count
#print limits



